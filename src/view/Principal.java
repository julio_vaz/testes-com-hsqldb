package view;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

public class Principal {
	Connection conexao;
	String driver = "org.hsqldb.jdbcDriver";
	String ulr = "jdbc:hsqldb:file:c:/testeHQSQL/testeHQSQL";
	String user = "SA";
	String pwd = "";

	public Connection getConexao() {
		if (conexao == null) {
			conectar();
			JOptionPane.showMessageDialog(null,
					"Alguma coisa errada n�o est� certa");
		}
		return conexao;
	}

	public void conectar() {
		try {
			Class.forName(driver);
			conexao = DriverManager.getConnection(ulr, user, pwd);
			JOptionPane.showMessageDialog(null, "Conex�o feita");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void conectar2() {
		try {
			this.ulr="jdbc:hsqldb:mem:.";
			Class.forName(driver);
			conexao = DriverManager.getConnection(ulr, user, pwd);
			JOptionPane.showMessageDialog(null, "Conex�o feita");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet executaConsulta(String sql) {
		try {
			Statement stmt = getConexao().createStatement();
			return stmt.executeQuery(sql);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void executaComando(String sql) {
		try {
			Statement stmt = getConexao().createStatement();
			stmt.execute(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Principal pri = new Principal();
		/*
		pri.conectar();
		ResultSet res = pri.executaConsulta("Select * from alunos");
		try {
			while (res.next()) {
				System.out.println(res.getString("nome"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}*/
		pri.conectar();
		Connection con1 = pri.getConexao();
		ResultSet res = pri.executaConsulta("Select * from alunos");
		try {
			System.out.println("\n\nPRIMEIRA RODADA\n\n");
			while (res.next()) {
				System.out.println(res.getString("nome"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		pri.conectar2();
		Connection con2 = pri.getConexao();
		res = pri.executaConsulta("Select * from alunos");
		try {
			System.out.println("\n\nSEGUNDA RODADA\n\n");
			while (res.next()) {
				System.out.println(res.getString("nome"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}